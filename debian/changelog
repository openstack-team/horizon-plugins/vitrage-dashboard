vitrage-dashboard (7.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090704).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 14:06:41 +0100

vitrage-dashboard (7.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 17:08:47 +0200

vitrage-dashboard (7.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 23:07:48 +0200

vitrage-dashboard (7.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 10:27:27 +0200

vitrage-dashboard (6.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Apr 2024 10:01:38 +0200

vitrage-dashboard (6.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Mar 2024 16:18:48 +0100

vitrage-dashboard (5.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 12:28:54 +0200

vitrage-dashboard (5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Sep 2023 11:19:31 +0200

vitrage-dashboard (4.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1045222).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 10:34:12 +0200

vitrage-dashboard (4.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:49:44 +0200

vitrage-dashboard (4.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 15:49:09 +0100

vitrage-dashboard (4.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends.
  * Removed django-4-ugettext_lazy-is-removed.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Mar 2023 09:32:16 +0100

vitrage-dashboard (3.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:09:09 +0200

vitrage-dashboard (3.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed django-4-django.conf.urls.url-is-removed.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Sep 2022 17:20:54 +0200

vitrage-dashboard (3.5.0-3) unstable; urgency=medium

  * Add Django 4 compat patches (Closes: #1015047):
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Sun, 31 Jul 2022 11:12:16 +0200

vitrage-dashboard (3.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 09:36:38 +0200

vitrage-dashboard (3.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Mar 2022 09:02:17 +0100

vitrage-dashboard (3.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:57:42 +0200

vitrage-dashboard (3.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Switch from testrepository to stestr in d/control.
  * (build-)depends on horizon version >= 3:20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Sep 2021 09:42:15 +0200

vitrage-dashboard (3.3.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:36:51 +0200

vitrage-dashboard (3.3.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 12:48:20 +0200

vitrage-dashboard (3.3.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:41:28 +0200

vitrage-dashboard (3.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 22:32:05 +0100

vitrage-dashboard (3.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Sep 2020 10:45:17 +0200

vitrage-dashboard (3.1.0-2) experimental; urgency=medium

  * d/control: Add missing dependency python3-xstatic-lodash

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 29 Apr 2020 22:56:03 +0200

vitrage-dashboard (3.1.0-1) experimental; urgency=medium

  * New upstream version
  * d/control:
    - Fix {build-}dependencies for this version
    - Bump debhelper-compat to 11
    - Bump standards version to 4.5.0

 -- Michal Arbet <michal.arbet@ultimum.io>  Sun, 26 Apr 2020 12:00:25 +0200

vitrage-dashboard (3.0.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * Move the package to the horizon-plugins subgroup on Salsa.

  [ Michal Arbet ]
  * New upstream version
  * d/control: Fix requirements for this release

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 24 Apr 2020 11:55:22 +0200

vitrage-dashboard (2.0.0-4) unstable; urgency=medium

  * Really package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled
    (last upload added no change).

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Jul 2021 09:58:01 +0200

vitrage-dashboard (2.0.0-3) unstable; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Jul 2021 15:00:09 +0200

vitrage-dashboard (2.0.0-2) unstable; urgency=medium

  * Rebuild source only.

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 05 Mar 2020 16:23:22 +0100

vitrage-dashboard (2.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #944659).

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 13 Nov 2019 13:41:51 +0100
